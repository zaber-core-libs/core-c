#include <stdio.h>

#include "za_serial.h"

int find_baud(z_device zd)
{
	int i;
	char reply[256];
	int bauds[] = {9600, 19200, 38400, 57600, 115200};
	int nbauds = sizeof(bauds)/sizeof(bauds[0]);

	for (i = 0; i < nbauds; i++) {
		za_setbaud(zd, bauds[i]);
		za_send(zd, "/\n");
		za_receive(zd, reply, 256);
		if (reply[0] == '@') {
#ifndef NDEBUG
			printf("Reply received: %s\n", reply);
#endif
			return bauds[i];
		}
	}
	return -1;
}

int main()
{
	z_device zd = za_connect("/dev/ttyUSB0");
	printf("Baud rate found: %d\n", find_baud(zd));

	return 0;
}
