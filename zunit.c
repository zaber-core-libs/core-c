/* Tests the tests! */
#include "zunit.h"

int test_should_pass()
{
    ASSERT_TRUE(1);
    return 0;
}

int test_should_fail()
{
    ASSERT_TRUE(0);
    return 0;
}

int test_equals()
{
    int x = 1;
    int y = 1;
    ASSERT_EQUALS(x, y);
    return 0;
}

int main()
{
    test_should_pass();
    test_should_fail();
    test_equals();

    return 0;
}
