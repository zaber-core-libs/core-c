.PHONY : tests clean release documentation docs

export TODAY=$(shell date +"%d %B %Y")
export VERSION=$(shell cat z_common.h | grep "\#define VERSION" | grep -E -o \[0-9\]+\.\[0-9\]+)
export RELEASE_FILES=example.c LICENSE.txt README.txt z_common.h za_serial.c za_serial.h zb_serial.c zb_serial.h
# REVISION is not used currently, but may be useful.
REVISION=$(shell svn info | awk '/Last Changed Rev:/ { print $$4 }')
ZIPFILE=zaber-core-serial-c-v${VERSION}.zip

release :
	@echo making release version ${VERSION}...
	@echo populating version numbers and dates...
	sed -i~ -e '1,10 s/$$(VERSION)/${VERSION}/' -e '1,10 s/$$(TODAY)/${TODAY}/' ${RELEASE_FILES}
	@echo making zip file...
	zip -r ${ZIPFILE} . -i ${RELEASE_FILES}
	@echo restoring original files...
	for f in ${RELEASE_FILES}; do { mv $$f~ $$f; } done
	@echo Uploading new version to website...
	scp ./${ZIPFILE} zaber@zaber.nmsrv.com:~/web-x64/website/source/software/core_c/${ZIPFILE}
	@echo done!

documentation : 
	doxygen Doxyfile

docs : documentation

tests : ascii-test binary-test

ascii-test :
	gcc -Wall -Wextra -std=c99 -pedantic -DNDEBUG -o ascii-test za_test.c za_serial.c

binary-test :
	gcc -Wall -Wextra -std=c99 -pedantic -DNDEBUG -o binary-test zb_test.c zb_serial.c

clean : 
	rm -rf ascii-test binary-test a.out *.o html latex

