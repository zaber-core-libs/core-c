\mainpage The Zaber Core Serial Library in C

\attention This library is deprecated and is no longer maintained.
Zaber no longer offers a C-language library. The closest modern alternative is the
[Zaber Motion Library](https://software.zaber.com/motion-library/docs), which
is available in C++.

The Zaber Core Serial Library is intended to provide a simple set of commands
for interacting with Zaber devices over a serial port. It is recommended that
before using this library, you first read the protocol manual corresponding to
the protocol of your device.

The [ASCII Protocol Manual]
(http://www.zaber.com/wiki/Manuals/ASCII_Protocol_Manual) is a complete
reference for communicating with Zaber devices in ASCII. Note that many
A-Series devices default to the binary protocol, and may need to be switched to
ASCII before use. [Appendix C](http://www.zaber.com/wiki/Manuals/AS
CII_Protocol_Manual#Appendix_C_-_Switching_between_Binary_and_ASCII_Protocols)
of the ASCII Protocol Manual is a short guide to doing this switch.

The [Binary Protocol Manual](http://www.zaber.com/wiki/Manuals/Binary_Protocol_
Manual) is a complete reference for communicating with Zaber devices in binary.
Note that devices using the binary protocol will only reply once a command has
completed, so more work must be done to properly receive responses. See the
["Move Absolute" command reference](http://www.zaber.com/wiki/Manuals/Binary_Pr
otocol_Manual#Move_Absolute_-_Cmd_20) and zb_set_timeout() for more info on
this.

##Downloading##

The officially published versions of the library can selected and
downloaded [here](https://api.zaber.io/downloads/software-finder?product=core_c)
(select the version you want then click the button),
or you can fork the source code and extend it yourself from the
[public repo](https://gitlab.com/zaber-core-libs/core-c).

##Getting Started##

The API is divided into two major parts:

* za_serial.h contains all functions required for communicating using the ASCII
protocol.
* zb_serial.h contains all functions required for communicating using the 
binary protocol.

There is also a third header, z_common.h. It defines a few things that both of
the other headers have in common. It should not be explicitly included in your
source files: it is already included by za_serial.h and zb_serial.h. 

Regardless of which protocol you are using, z_common.h defines a `"z_port"`
type, which is used to represent a connected port. A variable of this type
should be populated using za_connect() or zb_connect(). These functions will
not only set your `z_port` to a valid file descriptor/handle, but will also
configure it to the proper settings for a Zaber device using your protocol of
choice. 

After opening your `z_port`, it will be the first parameter to nearly every
other function exposed by this API, except for functions which do not interact
directly with your device (eg. zb_decode()). Note that with the exception of
za_connect() and zb_connect(), a `z_port` is passed by value, not by reference.

