#include <stdio.h>
#include <string.h>

#include "za_serial.h"
#include "zunit.h"

#if defined(__unix__)
#define Z_PORT "/dev/ttyUSB0"
#elif defined(__APPLE__)
#define Z_PORT "/dev/cu.usbserial-A4017CQY"
#else
#define Z_PORT "COM3"
#endif

/* 
 * Make sure connect succeeds. This test will only pass if a device
 * is actually connected.
 */
void test_connect()
{
	z_port zd;

	ASSERT_TRUE(za_connect(&zd, Z_PORT) == Z_SUCCESS && zd > 0);
	za_disconnect(zd);

	ASSERT_TRUE(za_connect(&zd, "Bad port name") == Z_ERROR_SYSTEM_ERROR);
	ASSERT_TRUE(za_connect(NULL, Z_PORT)  == Z_ERROR_NULL_PARAMETER);
	ASSERT_TRUE(za_connect(&zd, NULL) == Z_ERROR_NULL_PARAMETER);
}

/*
 * Make sure the reply decoding function works: test it against a typical
 * reply string and make sure everything gets sorted out the right way into
 * a za_reply struct.
 */
void test_decode_reply()
{
	unsigned int i;
	struct za_reply rep;
	char *bad_replies[] = { 
		"@01 0 OK IDLE --", 
		" @01 0 OKIDLE adsa -- ",
		"\0",
		"!!!!",
		"#This text may pass, let's see what happens.",
		"@100 40 NOT OK -- CERTAINLY NOT IDLE -- -- -- 2343"
	};

	char *good_replies[] = {
		"@01 0 OK IDLE -- 100 300 aaa",
		"!01 0 IDLE --",
		"#01 0 Visit www.zaber.com/support for complete instruction manuals."
	};

	ASSERT_EQUAL(za_decode(NULL, good_replies[0]), Z_ERROR_NULL_PARAMETER);
	ASSERT_EQUAL(za_decode(&rep, NULL), Z_ERROR_NULL_PARAMETER);

	for (i = 0; i < sizeof(bad_replies) / sizeof(char *); i++)
	{
		ASSERT_EQUAL(za_decode(&rep, bad_replies[i]), 
				Z_ERROR_COULD_NOT_DECODE);
	}

	for (i = 0; i < sizeof(good_replies) / sizeof(char *); i++)
	{
		ASSERT_EQUAL(za_decode(&rep, good_replies[i]), Z_SUCCESS);
		printf("Decoded reply: \n"
				"message type: %c\n"
				"device address: %d\n"
				"axis number: %d\n"
				"reply flags: %s\n"
				"device status: %s\n"
				"warning flags: %s\n"
				"response data: %s\n",
				rep.message_type,
				rep.device_address,
				rep.axis_number,
				rep.reply_flags,
				rep.device_status,
				rep.warning_flags,
				rep.response_data);
	}

	za_decode(&rep, good_replies[0]);
	ASSERT_TRUE(rep.message_type == '@'
			&& rep.device_address == 1
			&& rep.axis_number == 0
			&& (strncmp(rep.reply_flags, "OK", 2) == 0)
			&& (strncmp(rep.device_status, "IDLE", 4) == 0)
			&& (strncmp(rep.warning_flags, "--", 2) == 0)
			&& (strncmp(rep.response_data, "100 300 aaa", 11) == 0));
}

/*
 * TODO: Redirect stdout to a buffer or string, check the value of
 * that buffer. Test a bunch of things that should only print messages
 * when things go wrong and the verbose flag is set.
 *
 * NB: Only use this test without the NDEBUG flag; it's meaningless with.
 */
void test_verbose()
{
	za_set_verbose(0);
	int ret = za_receive(-1, NULL, 0); /* this should fail, but print no info */
	ASSERT_FALSE(ret == Z_SUCCESS);
	za_set_verbose(1);
}

/* 
 * Make sure za_send returns the number of bytes written.
 */
void test_send_retval()
{
	z_port zd;
	int ret;
	char *msg = "/\n";

	za_connect(&zd, Z_PORT);
	ret = za_send(zd, msg);
 	/* it's OK to cast to unsigned here since msg is not some enormous number
	 * that may be confused with a negative one (ie. an error code). */
	ASSERT_EQUALS((unsigned int) ret, strlen(msg));
	za_disconnect(zd);
}

/*
 * Make sure za_send returns early with ERROR_NULL_PARAMETER if given
 * a NULL parameter.
 */
void test_send_null_param()
{
	ASSERT_EQUALS(za_send(-1, NULL), Z_ERROR_NULL_PARAMETER);
}

/*
 * Make sure za_receive returns the number of bytes read.
 */
void test_receive_retval()
{
	z_port zd;
	int br;
	char buf[256];
	
	za_connect(&zd, Z_PORT);
	za_send(zd, "/\n");
	br = za_receive(zd, buf, 256);
	ASSERT_EQUALS((int)strlen(buf), br);
	za_disconnect(zd);
}

/*
 * Try to za_receive() into a too-small buffer. Make sure za_receive returns
 * -1. The buffer ought to still contain what of the message it could contain,
 *  but this is undocumented and therefore untested.
 */
void test_receive_buf_too_small()
{
	z_port zd;
	char too_small[3];
	int ret;

	za_connect(&zd, Z_PORT);
	za_send(zd, "/home\n");
	ret = za_receive(zd, too_small, 3);
	
	ASSERT_EQUALS(ret, Z_ERROR_BUFFER_TOO_SMALL);
	
	za_drain(zd); /* don't corrupt other tests' input */
	za_disconnect(zd);
}

/*
 * Currently only tests that za_setbaud() reports that it succeeded, which
 * is better than nothing. TODO: conditionally compile calls to tcgetattr()
 * or GetCommState() and verify the baud rate independently.
 */
void test_setbaud()
{
	z_port zd;
	int ret;

	za_connect(&zd, Z_PORT);
	ret = za_setbaud(zd, 57600);

	ASSERT_EQUALS(ret, Z_SUCCESS);

	za_disconnect(zd);
}

/*
 * Try sending two commands before reading their replies. Make sure two
 * replies are then read separately. 
 */
void test_multiple_commands()
{
	z_port zd;
	char buf1[256];
	char buf2[256];
	int neq;

	za_connect(&zd, Z_PORT);

	za_send(zd, "/\n");
	za_send(zd, "/\n");
	
	za_receive(zd, buf1, 256);
	za_receive(zd, buf2, 256);
	
	neq = strcmp(buf1, buf2);
	ASSERT_FALSE(neq);

	if(neq)
	{
		puts(buf1);
		puts(buf2);
	}
	za_disconnect(zd);
}

/* 
 * Send a bunch of messages, then drain all the replies. Make sure that
 * a call to za_receive() really does fail to get any more messages.
 */
void test_drain()
{
	z_port zd;
	char buf[256];

	za_connect(&zd, Z_PORT);
	za_send(zd, "/\n");
	za_send(zd, "/\n");
	za_send(zd, "/\n");
	za_send(zd, "/\n");
	za_send(zd, "/\n");
	za_send(zd, "/\n");
	za_drain(zd);
	ASSERT_TRUE(za_receive(zd, buf, 256) == Z_ERROR_SYSTEM_ERROR);
	puts(buf);
	za_disconnect(zd);
}

/*
 * TODO: Make "setup" and "teardown" functions, register tests as function
 * pointers, iterate through them, running setup and teardown in between.
 */
int main()
{
	/* first home the device, and receive a reply. This is a decent informal
	 * test of simple usage of the API: if there are errors here, Z_PORT is 
	 * either incorrect or there are serious problems with the API. */
	z_port zd;
	char *cmd = "/home\n";
	char reply[256] = { 0 };

	za_connect(&zd, Z_PORT);
	if (zd < 0)
	{
		puts("[ERROR] Device not found: likely not connected.\n");
		return 0;
	}
	za_send(zd, cmd);
	za_receive(zd, reply, 256);
	puts(reply);

	za_disconnect(zd);

	/* begin tests */

	/* These tests don't require interaction with a device. */
	test_decode_reply();
	test_verbose();
	test_send_null_param();

	if (reply[0] == '\0')
	{
		puts("[ERROR] could not read a reply from device. "
				"It is likely in Binary mode.\n");
		return 0;
	}

	/* only run these tests if a device is plugged-in */
	test_connect();
	test_send_retval();
	test_receive_retval();
	test_receive_buf_too_small();
	test_setbaud();
	test_multiple_commands();
	test_drain();

	return 0; 
}

