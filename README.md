# Zaber C Serial Library

## About

This repository contains the source code for Zaber's public C interface library.
See the Software section of the FWEE wiki for maintenance and distribution information.

C++ users should consider using the new
Zaber Motion Library (for the [ASCII Protocol](https://www.zaber.com/software/docs/motion-library/ascii/tutorials/install/cpp/),
or for the [Binary Protocol](https://www.zaber.com/software/docs/motion-library/binary/tutorials/install/cpp/)) instead,
as it provides more features and more robust protocol handling. 

No further development of the Zaber C Serial Library is planned, but existing
users will still be supported.

