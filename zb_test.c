#if defined(__unix__)
#define _POSIX_C_SOURCE 199309L
#define _BSD_SOURCE
#define Z_PORT "/dev/ttyUSB0"
#elif defined(__APPLE__)
#define _BSD_SOURCE
#define Z_PORT "/dev/cu.usbserial-FTDBI7DM"
#else
#define Z_PORT "COM3"
#endif


#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "zb_serial.h"
#include "zunit.h"

/* 
 * Make sure connect succeeds. This test will only pass if a device is
 * actually connected.
 */
void test_connect()
{
	z_port zd;
	int ret;

	ret = zb_connect(&zd, Z_PORT);
	ASSERT_TRUE(zd > 0 && ret == Z_SUCCESS);
}

/*
 * Make sure the "encode" function works. Gives a somewhat funny edge 
 * case and ensures it is encoded properly.
 */
void test_encode_command()
{
	uint8_t cmd[6];
	zb_encode(cmd, 1, 1, -1 );
	ASSERT_EQUALS(cmd[0], 1);
	ASSERT_EQUALS(cmd[1], 1);
	ASSERT_EQUALS(cmd[2], 0xff);
	ASSERT_EQUALS(cmd[5], 0xff);
}

/*
 * Make sure a (canned) reply is decoded properly. This test may fail verbatim
 * on a weirder architecture than x86, even though decode may be working 
 * properly. Be sure to tailor the ASSERT_EQUALS condition to what you would
 * expect on your machine.
 */
void test_decode_command()
{
	uint8_t rep[6] = { 1, 0, 255, 1, 2, 3 };
	int data;
	zb_decode(&data, rep);
	ASSERT_EQUALS(data, 3 << 24 | 2 << 16 | 1 << 8 | 255);
}

/* 
 * Make sure zb_send() returns 6 as it should.
 */
void test_send_retval()
{
	z_port zp;

	zb_connect(&zp, Z_PORT);
	uint8_t cmd[6] = { 1, 55, 1, 2, 3, 4 };

	ASSERT_EQUAL(6, zb_send(zp, cmd));

	zb_disconnect(zp);
}

/*
 * Make sure zb_receive() returns 6 as it should.
 */
void test_receive_retval()
{
	z_port zp;
	
	zb_connect(&zp, Z_PORT);
	uint8_t cmd[6];
	uint8_t rep[6];

	zb_encode(cmd, 1, 51, 0);
	zb_send(zp, cmd); 
	ASSERT_EQUAL(6, zb_receive(zp, rep));

	zb_disconnect(zp);
}

/*
 * Make sure that no data is left in the input buffer after zb_drain() has
 * been called. Send a bunch of messages without getting their replies,
 * call drain, then try to read a reply. If we succeed in reading a reply,
 * then drain failed.
 */
void test_drain()
{
	z_port zp;
	uint8_t cmd[6] = { 1, 55, 0, 0, 0, 0 };
	uint8_t rep[6] = { 0 };

	zb_connect(&zp, Z_PORT);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);
	zb_send(zp, cmd);

	zb_drain(zp);
	zb_receive(zp, rep);
	ASSERT_EQUAL(0, rep[0]);

	zb_disconnect(zp);
}

/*
 * Tests whether a new timeout genuinely takes effect. Roughly times how long
 * it takes for zb_receive() to give up on receiving a reply after setting the
 * timeout to something much smaller than the default 2s.
 */
void test_set_timeout()
{
#if defined(__unix__)
	z_port zp;
	struct timespec ts_before, ts_after;

	zb_connect(&zp, Z_PORT);

	clock_gettime(CLOCK_REALTIME, &ts_before);
	zb_receive(zp, NULL);
	clock_gettime(CLOCK_REALTIME, &ts_after);
	printf("Receive took %ld seconds and %ld nanoseconds.\n",
			(long) ts_after.tv_sec - ts_before.tv_sec,
			(long) ts_after.tv_nsec - ts_before.tv_nsec);

	zb_set_timeout(zp, 10000);
	puts("Set timeout to 10000ms (10s)\n");

	clock_gettime(CLOCK_REALTIME, &ts_before);
	zb_receive(zp, NULL);
	clock_gettime(CLOCK_REALTIME, &ts_after);
	printf("Receive took %ld seconds and %ld nanoseconds.\n",
			(long) ts_after.tv_sec - ts_before.tv_sec,
			(long) ts_after.tv_nsec - ts_before.tv_nsec);

	zb_set_timeout(zp, 2000);
	puts("Set timeout back to default of 2s\n");

	zb_disconnect(zp);
#endif /* if defined(__unix__) */
}
/*
 * Previous testing shows that the input buffer is about 3968 bytes large 
 * (or 661 messages) in Linux Mint 17, on a Dell OptiPlex 980. 
 * On a Windows VM given 2GB of RAM on the same OptiPlex, this test failed
 * much sooner, at just 84 messages (suggesting the buffer isn't much larger
 * than 504 bytes).
 *
 * Furthermore, each OS failed at a different place in this test:
 * Windows failed to read enough bytes at the first TEST_FAILED condition, 
 * while Linux succeeded in reading 6 bytes, but read the wrong bytes 
 * (failing at the second TEST_FAILED). 
 * Therefore both of these TEST_FAILED checks are necessary.
 */
void test_input_buffer_length(int nmessages)
{
	int i, j;
	z_port zd;
	
	zb_connect(&zd, Z_PORT);
	uint8_t cmd[6] = { 1, 55, 1, 2, 3, 4 };
	uint8_t rep[6] = { 0 };

	for (i = 0; i < nmessages; i++)
		zb_send(zd, cmd);

	for (i = 0; i < nmessages; i++)
	{
		if (zb_receive(zd, rep) != 6)
		{
			TEST_FAILED("Did not read full reply");
			zb_disconnect(zd);
			return;
		}
		for(j = 0; j < 6; j++)
		{
			if (rep[j] != cmd[j])
			{
				TEST_FAILED("bad reply");
				zb_disconnect(zd);
				return;
			}
		}
	}

	TEST_PASSED("sent and received all messages");
	zb_disconnect(zd);
}

/* 
 * A stress-test to check for bad reads: there were lots of problems in
 * development with both *NIX and Windows versions of zb_receive() reading
 * erroneous null bytes instead of actual data, which would then shift all
 * further replies over by the number of incorrect '\0' bytes read.
 * This test makes sure that doesn't happen any more.
 * */
void spam_and_check()
{
	int i, j;
	z_port zp;
	zb_connect(&zp, Z_PORT);
	uint8_t cmd[6] = {1, 55, 1, 2, 3, 4},
	rep[6] = { 0 };
	
	for(i = 0; i < 20; i++)
	{
        usleep(100000); /* 0.1s */
		zb_send(zp, cmd);
        usleep(100000); /* 0.1s */
		if (zb_receive(zp, rep) < 6) 
		{
			printf("Response %d failed to read a full response.\n"
			"Read: %d %d %d %d %d %d\n",  i, rep[0],
						rep[1], rep[2], rep[3], rep[4], rep[5]);
		}
		else 
		{	
			for (j = 0; j < 6; j++)
			{
				if (cmd[j] != rep[j])
				{
					printf("Response %d differs: %d %d %d %d %d %d\n",
							i, rep[0], rep[1], rep[2], rep[3], rep[4], rep[5]);
					break;
				}
			}
		}
		rep[0] = 0;
		rep[1] = 0;
		rep[2] = 0;
		rep[3] = 0;
		rep[4] = 0;
		rep[5] = 0;
		/* printf("Spam and check pass %d completed successfully.\n", i); */
	}

	/* an abuse of ASSERT_TRUE, but if we made it this far, we passed. */
	ASSERT_TRUE(1);
}
	

int main()
{
	z_port zp;
	uint8_t cmd[6];
	uint8_t rep[6] = { 0 };

	/* Home the device and try to get a reply. 
	 * The results of this test dictate whether further tests will be run. */
	zb_connect(&zp, Z_PORT);
	if (zp < 0)
	{
		printf("[ERROR] Device \"%s\" not found: likely not connected.\n",
                Z_PORT);
		return 0;
	}
	zb_encode(cmd, 1, 1, 0);
	zb_send(zp, cmd);
	zb_receive(zp, rep);
	printf("Received %d %d %d %d %d %d\n", 
			rep[0], rep[1], rep[2], rep[3], rep[4], rep[5]);
	zb_disconnect(zp);
	
	/* begin tests */

	/* these tests don't require interaction with a device. */
	test_encode_command();
	test_decode_command();

	if (rep[0] == '\0')
	{
		puts("[ERROR] Could not read a reply from device. "
				"It is likely in ASCII mode.\n");
		return 0;
	}

	/* these tests require a present, responsive device. */
	test_connect();
	test_send_retval();
	test_receive_retval();
	test_drain();
	test_set_timeout();
	test_input_buffer_length(100);
	spam_and_check();

	return 0;
}
	
