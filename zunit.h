#ifndef __zunit_h__
#define __zunit_h__

#include <stdio.h>

#ifdef HIDE_PASSED_TESTS
#define TEST_PASSED(M)  
#else
#define TEST_PASSED(M) fprintf(stderr, "[OK] (%s: %d) Test \"%s\" "\
        "passed test \"" M "\" \n",\
        __FILE__, __LINE__, __func__)
#endif
#define TEST_FAILED(M) fprintf(stderr, "[ERROR] (%s: %d) Test \"%s\" "\
        "failed test \"" M "\" \n",\
        __FILE__, __LINE__, __func__)

#define ASSERT_TRUE(A) do { if (!(A)) { TEST_FAILED(#A); }\
    else { TEST_PASSED(#A); } } while(0)
#define ASSERT_FALSE(A) do { if (A) { TEST_FAILED(#A); }\
    else { TEST_PASSED(#A); } } while(0)
#define ASSERT_EQUALS(A, B) ASSERT_TRUE((A) == (B))
#define ASSERT_EQUAL(A, B) ASSERT_EQUALS(A, B)

#endif

